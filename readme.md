API for rubber ducks

I used here a thin framework for routing paths and for parsing http requests.

    Installation:

    1. php composer to install the required thin structure and the required dependency

    2. create your own database for the project and create the duckTable.sql file for storing ducks

    3. Change the name of config.php.default to config.php and enter the required value


A simple REST API for CRUD operations on rubber ducks

HOW USE IT

getting ducks:
    on GET request, the path:

    /duck/get/:limit 

    is optional (default limited to the last 10 ducks) the answer is an array of JSON objects.

    Example: {"id": "20", "first name": "cyberDuck", "price": "1233.21", "created": "2018-06-24 18:01:59"}

adding a duck
    is carried out by the POST request on the path 
    
    /duck/insert

    and the parameters are sent over the content of the post, example:

        name: cyberDuck
        Price: 1233.21

    the answer is a JSON object with the key "msg" on success or "err" when something goes wrong

remove the duck
    Request DELETE, path to 
    
    /duck/del/:id

    id is the duck identifier

    the answer is a JSON object with the 'msg' key about success with the ID number of the deleted duck or 'err' when something goes wrong

edit the duck
    api can edit the name or price of the duck on PUT request:

    /duck/edit/name/:id/:name 

    id to edit duck (we can only use numbers) and name (only text and lower case letters)
    

    /duck/edit/price/:id/:price 

    id same as above, and is a floating point number with a dot

All other parameters in the URL will give us 404 - just check the correctness of the parameters.


TESTING
    
    For testing I use postman, but is easy to write some test for example in curl. 
