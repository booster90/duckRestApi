<?php

require_once('db.php');
require 'vendor/autoload.php';

$app = new Slim\App();

$app->get('/', function($request, $response, $args) {
    return $response->withStatus(200)->write('rubber ducks API');
});

/**
 * get all ducks or limit
 */
$app->get('/ducks/get[/{limit:[0-9]+}]', function($request, $response, $args) {
    $db = new db();
    
    if (isset($args['limit'])) {
        $ret = $db->getDucks($args['limit']);
    } else {
        $ret = $db->getDucks();
    }

    return $response->withJson($ret, 200);
});

/**
 * add duck over post request
 */
$app->post('/duck/insert', function($request, $response, $args) {
    if (isset($request->getParsedBody()['name']) && isset($request->getParsedBody()['price'])) {
        $db = new db();
        
        $res = $db->addDuck($request->getParsedBody());

        return $response->withJson($res, 200);
    } else {
        return $response->withJson(['err' => 'No parameters'], 200);
    }
});

/**
 * delete duck :(
 */
$app->delete('/duck/del/{id:[0-9]+}', function($request, $response, $args) {
    $db = new db();
    $res = $db->delDuck($args['id']);

    return $response->withJson($res, 200);
});

/**
 * edit duck name
 */
$app->put('/duck/edit/name/{id:[0-9]+}/{name:[a-zA-Z]+}', function($request, $response, $args) {
    $db = new db();
    $res = $db->updateDuckName($args['id'], $args['name']);

    return $response->withJson($res, 200);
});

/**
 * edit duck price
 */
$app->put('/duck/edit/price/{id:[0-9]+}/{price:[0-9.]+}', function($request, $response, $args) {
    $db = new db();
    $res = $db->updateDuckPrice($args['id'], $args['price']);

    return $response->withJson($res, 200);
});

// Run application
$app->run();
