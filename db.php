<?php
/**
 * Class for connection with database 
 *
 * @author krystian
 */

require_once 'config.php';

class db {
    private $servername = 'localhost',
        $username = '',
        $password = '',
        $dbname = '',
        $conn = null;

    public function __construct() {
        $this->servername = HOST;
        $this->username = USER;
        $this->password = PASS;
        $this->dbname = DB_NAME;
        
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            return ['err' => $e->getMessage()];
        }
    }
    
    /**
     * Add duck to database
     * 
     * @param array $duck
     * @return type
     */
    public function addDuck(array $duck) {
        $stmt = $this->conn->prepare("INSERT INTO duck (name, price) VALUES (:name, :price)");

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':price', $price);

        // insert a row
        $name = $duck['name'];
        $price = $duck['price'];

        return $stmt->execute() ? ['msg' => 'row added'] : ['err' => 'some problem occurs'];
    }

    /**
     * Get ducks
     * 
     * @param type $limit
     * @return type
     */
    public function getDucks($limit = 10) {
        if ($limit === 10) {
            
            try {
                $stmt = $this->conn->prepare("SELECT id, name, price, created FROM duck ORDER BY id DESC LIMIT 10");
                $stmt->execute();
            } catch (PDOException $e) {
                return ['err' => $e->getMessage()];
            }
            
        } else {
            
            try {
                $stmt = $this->conn->prepare("SELECT id, name, price, created FROM duck ORDER BY id DESC LIMIT :limit");
                $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
                $stmt->execute();
            } catch (PDOException $e) {
                return ['err' => $e->getMessage()];
            }
            
        }

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Delete duck by id
     * 
     * @param type $id
     * @return type
     */
    public function delDuck($id) {
        try {
            $stmt = $this->conn->prepare("DELETE FROM duck WHERE id=:id");
            $stmt->bindValue(':id', (int)$id, PDO::PARAM_INT);

            return $stmt->execute() ? ['msg' => 'duck id = ' . $id . ' was deleted'] : ['err' => 'some error occurs during delete id = ' . $id];
        } catch (PDOException $e) {
            return ['err' => $e->getMessage()];
        }
    }

    /**
     * Update duck name 
     * 
     * @param type $id
     * @param type $name
     * @return type
     */
    public function updateDuckName($id, $name) {
        try {
            $stmt = $this->conn->prepare('UPDATE duck SET name=:name WHERE id=:id');
            $stmt->bindValue(':id', (int)$id, PDO::PARAM_INT);
            $stmt->bindValue(':name', $name, PDO::PARAM_STR);
            
            return $stmt->execute() ? ['msg' => 'duck id = ' . $id . ' was edited'] : ['err' => 'some error occurs during edit id = ' . $id];
        } catch (PDOException $e) {
            return ['err' => $e->getMessage()];
        }
    }
    
    /**
     * Update duck price
     * 
     * @param type $id
     * @param type $price
     * @return type
     */
    public function updateDuckPrice($id, $price) {
        try {
            $stmt = $this->conn->prepare('UPDATE duck SET price=:price WHERE id=:id');
            $stmt->bindValue(':id', (int)$id, PDO::PARAM_INT);
            $stmt->bindValue(':price', $price, PDO::PARAM_STR);
            
            return $stmt->execute() ? ['msg' => 'duck id = ' . $id . ' was edited'] : ['err' => 'some error occurs during edit id = ' . $id];
        } catch (PDOException $e) {
            return ['err' => $e->getMessage()];
        }
    }
}
